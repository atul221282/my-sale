import { Column, CreateDateColumn, Entity, UpdateDateColumn, VersionColumn, OneToMany } from 'typeorm';
import { ApiModelPropertyOptional } from '@nestjs/swagger';
import { IsString, IsArray } from 'class-validator';
import { Base } from '../core/entities/base.entity';
import { Exclude } from 'class-transformer';
import { UsersToken } from './user-token.entity';

@Entity('user')
export class User extends Base {
  @ApiModelPropertyOptional({ type: String })
  @IsString()
  @Column({ nullable: true })
  name?: string;

  @CreateDateColumn({ type: 'timestamptz' })
  createdAt?: Date;

  @UpdateDateColumn({ type: 'timestamptz' })
  updatedAt?: Date;

  @Exclude()
  @VersionColumn()
  version?: number;

  @OneToMany(_ => UsersToken, token => token.user)
  @IsArray()
  tokens: UsersToken[];
}
