import { ApiModelProperty } from '@nestjs/swagger';
import { Base } from '../core/entities/base.entity';
import { Column, ManyToOne, Entity } from 'typeorm';
import { User } from './user.entity';

@Entity('userToken')
export class UsersToken extends Base {
  @ApiModelProperty()
  @Column({
    nullable: false,
  })
  token: string;

  @ApiModelProperty()
  @ManyToOne(type => User, user => user.tokens, { onDelete: 'CASCADE', nullable: false })
  user: User;
}
